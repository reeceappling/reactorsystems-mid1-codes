const coreThermalOutput = 2786; //MWt
const P = 1045;//psia
const Tfeed = 440; //degF
const S = 0.64; //in
const Dfuel = 0.493; //in
const Dwater = 0.591;//in
const H = 150;//in
const Fz = 1.4;
const Fq = 2.22;
const Acan = 5.278**2;//in^2
const lattice = 8*8;
const NA = 560;
const waterRodsPerAssemb = 2;
const rodLengthWithPlenum = 160;//in
const tiePlatesLossCoeff = 1.5;
const nGrids = 5;
const gridLossCoeff = 0.5;
const gamma = 0.94;
const minCPR = 1.2;

const pi = Math.PI;


const Pc = 3208.2;//psia, critical pressure for water
const Gstar = 3375*((1-(P/Pc))**3);

//record hFeed
const hfeed = 419.445;//Btu/lbm from P and Tfeed in steamtables

//set saturation properties
const rhof = 45.9519;//lb/ft3
const hf = 549.498;//Btu/lbm
const muf = 0.218924;//lb/ft hr
const kf = 0.328564;//Btu/hr ft degF
const rhog = 2.35527;//lb/ft3
const hg = 1190.85;//Btu/lbm
const mug = 0.0460845;//lb/ft hr
const kg = 0.0368286;//Btu/hr ft degF
const hfg = hg-hf;

let xmax = 2.029; //position of maximum heat flux (see hw2p3a)

//find mFeed
const Q = coreThermalOutput*3412141.16;//Qdot in Btu/hr
let mFeed = Q/(hg-hfeed);//Calculated using second eqn from problem
console.log("Feed mass flow lbm/hr: ",mFeed);
//set mdot
let mCore = 7*(10**2);//lbm/hr, SET CORE MASS FLOW RATE HERE!!!--------------------------------------------------------
let ABund = Acan-((pi/4)*((2*(Dwater**2))+((lattice-2)*(Dfuel**2))));
let Acore = ABund*NA;
let nRods = NA*(lattice-waterRodsPerAssemb);
let mCh = mCore/nRods;
//find hin
let hin = ((mFeed*hfeed)+(hf*(mCore-mFeed)))/mCore;
let hsub = hf-hin;
//find lambda
function lambdaFunc(L){
	let templ = L;
	let tmpHe = (2*templ)+H;
	let temp2 = pi*(L)/(H+(2*L))
	return ((xmax*Math.sin(xmax))/((1/H)*((templ*Math.cos(pi*templ/tmpHe))-((H+templ)*Math.cos(pi*(H+templ)/tmpHe))-((tmpHe/pi)*Math.sin(pi*templ/tmpHe))+((tmpHe/pi)*Math.sin(pi*(H+templ)/tmpHe)))))-Fz;
}
let templambda = 0;
let toTest =0;
const iterationValue = 100;
for(var i=0;i<1000;i++){
	let toTest = lambdaFunc((1/1000)*i);
	if(Math.abs(toTest)<Math.abs(lambdaFunc(templambda))){
		templambda = Math.abs(toTest);
	}
}
console.log("Lambda found: ",templambda,", Closeness: ",Math.abs(toTest));
//find He
let lambda = templambda;
let He = (2*lambda)+H;
//find qo/hot?
let qavg = Q*gamma/(nRods*pi*Dfuel);console.log("qavg= ",qavg);
let qmax = qavg*Fq;console.log("qmax = ",qmax);
let qo = qmax/(xmax*Math.sin(xmax));
let qohot = Fq*qo/Fz;console.log("qohot= ",qohot);
//get enthalpy distribution
function h(z,qovalue){
	let const1 = (pi*(H+lambda-z))/He;
	let const2 = (pi*(H+lambda))/He;
	return (hin+((qovalue*(He/12)*(Dfuel/12)/(mCh*gamma))*((const1*Math.cos(const1))-(const2*Math.sin(const2))-Math.sin(const1)+Math.sin(const2))));
};
//find Ho
let Ho = 0;
for(i=0;i<1000;i++){
	if(Math.abs(h((H/1000)*i,qohot)-hf)<Math.abs(h(Ho,qohot)-hf)){
		Ho = (H/1000)*i;
	}
}
console.log("Ho in hot channel found to be (inches) ",Ho);
//Check CPR
let Dh = Dfuel;
let De = (4*((S**2)-(pi*(Dh**2)/4)))/(pi*Dh);
let G = mCore/Acore;
let Gmetr = G*0.001356229913;
let b = 0.199*(((Pc/P)-1)**0.4)*Gmetr*(Dh**1.4);
let a=0;
if(Gmetr<=3375*((1-(P/Pc))**3)){
	a=1/(1+(1.481*(10**-4)*Gmetr*((1-(P/Pc))**-3)));
}
else{
	a=(1-(P/Pc))/((Gmetr/1000)**(1/3));
}
a = a*Dh/De;
let eqna = lambda*pi/He;
let eqnb = (pi/He)*(H+lambda-Ho);
eqna = (eqna*Math.cos(eqna))-(eqnb*Math.cos(eqnb))-Math.sin(eqna)+Math.sin(eqnb);
eqna = eqna*He*Dh/(hfg*gamma*mCh);
function qocrit(Hocrit){
	let fxn1 = (pi/He)*(H+lambda-Hocrit);
	let fxn2 = pi*(H+lambda)/He;
	return ((mCh*gamma*hsub)/(He*Dh*((fxn1*Math.cos(fxn1))-(fxn2*Math.cos(fxn2))-Math.sin(fxn1)+Math.sin(fxn2))));
}
let Hocrit = 0;
a = a*(H-Ho)/(H-Ho+b);
for(i=0;i<1000;i++){
	let Hotest = (H/1000)*i;
	let teVal = (qocrit(Hotest)*eqna)-a;
	if(Math.abs(teVal)<Math.abs((qocrit(Hocrit)*eqna)-a)){
		Hocrit = Hotest;
	}
}
console.log("Hocrit found to be ",Hocrit);
let qocr = qocrit(Hocrit);
console.log("CPR is then ",qocr/qohot," Aim for Min ",minCPR);

